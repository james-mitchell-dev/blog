---
title: "RG-280V Review"
date: 2021-10-09T04:25:04-04:00
draft: true
---

Earlier this year I purchased an RG-280V. Here are my thoughts. 

### Hardware

|                |                                                   |
| :------------- | ------------------------------------------------: |
| CPU            |                               JZ4770 dual 1.0 GHz |
| RAM            |                                         DDR2 512M |
| Screen         | 2.8 inch IPS screen, OCA  full lamination/320*480 |
| Size           |                                89mm x 78mm x 18mm |
| Battery        |                               Li-polymer 2100 mAh |
| Speaker        |                 High quality  horn stereo speaker |
| Color          |                                     Golden/Silver |
| TF card        |                              Support max to 256GB |
| Other function |                                         vibrating |

