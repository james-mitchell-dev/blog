---
title: "Please Ignore"
date: 2021-10-08T03:51:45-04:00
tags: ["Programming", "C++", "TestPost"]
showToc: true
draft: false
cover:
    image: "https://via.placeholder.com/800x200" # image path/url
    alt: "800x200" # alt text
    caption: "800x200" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tincidunt nunc quis urna rhoncus tristique. Vestibulum aliquam, ipsum sit amet egestas finibus, diam ipsum bibendum nulla, vel interdum ex risus id odio. Etiam pellentesque massa ultricies lectus faucibus tempor eu ut mi. Aliquam sit amet mi et odio volutpat elementum at laoreet neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam semper scelerisque elit, sit amet ullamcorper lorem egestas in. Sed accumsan, lorem id hendrerit placerat, sem lectus vehicula urna, id tincidunt lorem metus at erat. Etiam quis metus pharetra, euismod quam imperdiet, lacinpia ligula. Nullam nec malesuada mi. Pellentesque scelerisque arcu id augue condimentum, at eleifend quam posuere. Praesent ipsum mauris, pulvinar vel dui mollis, hendrerit elementum nunc. Quisque non ligula varius sapien tristique posuere sit amet eu massa. 

```cpp
#include <iostream>
using namespace std;

int main() 
{
    cout << "Hello, Blog!\n";
    return 0;
}
```